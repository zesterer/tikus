/*
* 	file : char.hpp
*
* 	This file is part of Tikus.
*
* 	Tikus is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tikus is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tikus.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIKUS_CHAR_HPP
#define TIKUS_CHAR_HPP

#include <tikus/syntaxtree.hpp>

namespace Tikus
{
	bool is_blank(char c) { return (c == ' ' || c == '\t' || c == '\n'); }
	bool is_digit(char c) { return (c >= '0' && c <= '9'); }
	bool is_alpha(char c) { return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')); }
	bool is_alphanum(char c) { return (is_alpha(c) || is_digit(c)); }
	bool is_identifier_head(char c) { return (is_alpha(c) || c == '_'); }
	bool is_identifier(char c) { return (is_alphanum(c) || c == '_'); }
	bool is_operator(char c) { return (c == '+' || c == '-' || c == '*' || c == '/' || c == '<' || c == '>' || c == '&' || c == '|' || c == '!' || c == '='); }
	bool is_parenthesis(char c) { return (c == '(' || c == ')' || c == '{' || c == '}' || c == '[' || c == ']'); }
}

#endif
