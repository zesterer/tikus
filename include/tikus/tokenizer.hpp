/*
* 	file : tokenizer.hpp
*
* 	This file is part of Tikus.
*
* 	Tikus is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tikus is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tikus.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIKUS_TOKENIZER_HPP
#define TIKUS_TOKENIZER_HPP

#include <tikus/token.hpp>

namespace Tikus
{
	class Tokenizer
	{
	private:
		TokenList tokenList;

	public:
		void tokenize(std::string text);
		TokenList& getList();
	};
}

#endif
