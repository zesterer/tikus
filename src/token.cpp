/*
* 	file : tokenizer.cpp
*
* 	This file is part of Tikus.
*
* 	Tikus is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tikus is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tikus.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tikus/token.hpp>

namespace Tikus
{
	Token::Token(Type type, filepos_t filepos, std::string str, filepos_t startpos, filepos_t endpos)
	{
		this->type = type;
		this->filepos = filepos;

		this->str = str.substr(startpos, endpos - startpos);
	}

	std::string Token::toStr()
	{
		std::string str = "";

		switch (this->type)
		{
		case Type::IDENTIFIER:
			str += "IDENTIFIER";
			break;

		case Type::STRING_LITERAL:
			str += "STRING_LITERAL";
			break;

		case Type::WHITESPACE:
			str += "WHITESPACE";
			break;

		case Type::NUMBER:
			str += "NUMBER";
			break;

		case Type::OPERATOR:
			str += "OPERATOR";
			break;

		case Type::PARENTHESIS:
			str += "PARENTHESIS";
			break;

		case Type::COMMA:
			str += "COMMA";
			break;

		default:
			str += "NONE";
			break;
		}

		str += " '" + this->str + "' at position " + std::to_string(this->filepos + 1);
		return str;
	}

	std::string TokenList::toStr()
	{
		std::string str = "TokenList:\n";

		for (size_t i = 0; i < this->tokens.size(); i ++)
			str += this->tokens[i].toStr() + "\n";

		return str;
	}

	void TokenList::clean()
	{
		std::vector<Token> ntokens;

		for (size_t i = 0; i < this->tokens.size(); i ++)
		{
			Token::Type type = this->tokens[i].type;

			if (type != Token::Type::WHITESPACE)
				ntokens.push_back(this->tokens[i]);
		}

		this->tokens = ntokens;
	}
}
