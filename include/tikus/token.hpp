/*
* 	file : tokenlist.hpp
*
* 	This file is part of Tikus.
*
* 	Tikus is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tikus is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tikus.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIKUS_TOKENLIST_HPP
#define TIKUS_TOKENLIST_HPP

#include <tikus/types.hpp>

#include <vector>
#include <string>

namespace Tikus
{
	struct Token
	{
		enum class Type
		{
			WHITESPACE,
			IDENTIFIER,
			STRING_LITERAL,
			NUMBER,
			OPERATOR,
			PARENTHESIS,
			COMMA,
			NONE,
		};

		filepos_t filepos = 0;
		Type type = Type::NONE;
		std::string str = "";

		Token(Type type, filepos_t filepos, std::string str, filepos_t startpos, filepos_t endpos);
		std::string toStr();
	};

	class TokenList
	{
	private:
		std::vector<Token> tokens;

	public:
		Token& get(size_t index) { return this->tokens[index]; }
		void add(Token t) { this->tokens.push_back(t); }
		std::string toStr();
		void clean();
	};
}

#endif
