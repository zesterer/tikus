#include <tikus/tokenizer.hpp>
#include <tikus/parser.hpp>

#include <stdio.h>
#include <stdlib.h>

void show_error(const char* msg)
{
	printf("tikus: error: %s\n", msg);
}

char* get_file_text(const char* filename)
{
	char* buffer = nullptr;

	// Attempt to open the file
	FILE* file = fopen(filename, "rb");

	if (file)
	{
		// Seek to the start and find length
		fseek(file, 0, SEEK_END);
		long len = ftell(file);
		fseek(file, 0, SEEK_SET);

		// Allocate buffer for the file text
		buffer = (char*)malloc(sizeof(char) * (len + 1));
		// Read file into buffer
		fread(buffer, 1, len, file);

		// Close the file
		fclose(file);

		return buffer;
	}
	else
	{
		show_error("could not open file");
		return nullptr;
	}
}

int main(int argc, char* argv[])
{
	if (argc <= 1)
	{
		show_error("no input files");
		return 1;
	}
	else
	{
		char* filetext = get_file_text(argv[1]);

		if (filetext == nullptr)
			return 1;
		else
		{
			Tikus::Tokenizer tokenizer;
			tokenizer.tokenize(std::string(filetext));

			printf("TokenList: %s", tokenizer.getList().toStr().c_str());
		}

		free(filetext);
	}

	return 0;
}
