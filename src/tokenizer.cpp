/*
* 	file : tokenizer.cpp
*
* 	This file is part of Tikus.
*
* 	Tikus is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Tikus is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Tikus.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tikus/tokenizer.hpp>
#include <tikus/char.hpp>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

namespace Tikus
{
	void Tokenizer::tokenize(std::string text)
	{
		//printf("TEXT: %s\n", text.c_str());

		Token::Type state = Token::Type::WHITESPACE;
		filepos_t pos = 0;
		filepos_t startpos = pos;
		bool end = false;

		while (!end)
		{
			bool recycle = false;
			bool end_token = false;
			bool include_start = true;
			bool include_end = false;
			Token::Type start_state = state;

			char c;
			if (pos >= text.length())
				c = '\0';
			else
				c = text[pos];

			if (c == '\0')
			{
				this->tokenList.add(Token(state, startpos, text, startpos, pos));
				end = true;
				break;
			}

			switch (state)
			{
			case Token::Type::WHITESPACE:
				{
					if (c == '\"')
					{
						end_token = true;
						state = Token::Type::STRING_LITERAL;
					}
					else if (is_digit(c))
					{
						end_token = true;
						state = Token::Type::NUMBER;
					}
					else if (c == ',')
					{
						end_token = true;
						recycle = true;
						state = Token::Type::COMMA;
					}
					else if (is_parenthesis(c))
					{
						end_token = true;
						recycle = true;
						state = Token::Type::PARENTHESIS;
					}
					else if (is_operator(c))
					{
						end_token = true;
						state = Token::Type::OPERATOR;
					}
					else if (is_identifier_head(c))
					{
						end_token = true;
						state = Token::Type::IDENTIFIER;
					}
				}
				break;

			case Token::Type::IDENTIFIER:
				{
					if (!is_identifier(c))
					{
						end_token = true;
						recycle = true;
						state = Token::Type::WHITESPACE;
					}
				}
				break;

			case Token::Type::STRING_LITERAL:
				{
					if (c == '\"')
					{
						end_token = true;
						include_start = false;
						state = Token::Type::WHITESPACE;
					}
				}
				break;

			case Token::Type::NUMBER:
				{
					if (!is_alphanum(c) || c == '.')
					{
						end_token = true;
						recycle = true;
						state = Token::Type::WHITESPACE;
					}
				}
				break;

			case Token::Type::OPERATOR:
				{
					if (!is_operator(c))
					{
						end_token = true;
						recycle = true;
						state = Token::Type::WHITESPACE;
					}
				}
				break;

			case Token::Type::PARENTHESIS:
				{
					end_token = true;
					include_end = true;
					state = Token::Type::WHITESPACE;
				}
				break;

			case Token::Type::COMMA:
				{
					end_token = true;
					include_end = true;
					state = Token::Type::WHITESPACE;
				}
				break;

			default: // Erroneous state
				return;
			};

			if (end_token)
			{
				filepos_t endpos = pos;

				if (!include_start)
					startpos ++;
				if (include_end)
					endpos ++;
				this->tokenList.add(Token(start_state, startpos, text, startpos, endpos));
				startpos = pos;
			}

			// Move to the next character
			if (!recycle)
				pos ++;
		}

		// Clean the tokens (i.e: remove whitespace)
		this->tokenList.clean();
	}

	TokenList& Tokenizer::getList()
	{
		return this->tokenList;
	}
}
